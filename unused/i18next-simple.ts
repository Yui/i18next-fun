import i18next from "https://deno.land/x/i18next@v20.2.2/index.js";
import Backend from "https://deno.land/x/i18next_fs_backend@v1.1.1/index.js";

export const namespaces = {
  common: "common",
};

export const languages = {
  cz: "cz",
  en: "en",
};

export const cz = {
  [namespaces.common]: {
    test: {
      ok: "Ok",
      cancel: "zrušit",
    },
  },
};

export const en = {
  [namespaces.common]: {
    test: {
      ok: "Ok",
      cancel: "Cancel",
    },
  },
};

const createI18n = (language: string) => {
  const i18n = i18next.createInstance().use(Backend);
  i18n.init({
    lng: language,
    fallbackLng: language,
    resources: {
      [languages.en]: en,
      [languages.cz]: cz,
    },
  });
  return i18n;
};

const i18n = createI18n(languages.en);

const map = i18n.getFixedT(languages.en, "common");

const translated = map("test.ok");

console.log(translated);
