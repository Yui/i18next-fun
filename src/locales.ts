export const Locales = {
  EN: "en",
  FR: "fr",
};

export const namespaces = {
  default: "default",
};

const english = {
  [namespaces.default]: {
    hi: {
      person: "hi {{name}}",
      group: "hi everyone",
    },
    dog: "puppie",
    test: "{{word}} {{action}}",
  },
} as const;

const france = {
  [namespaces.default]: {
    person: "hi in france, {{name}}",
    group: "hi in france everyone",
  },
} as const;

export const messages = {
  [Locales.EN]: english,
  [Locales.FR]: france,
};

export const defaultLocale = Locales.EN;

export const defaultLocaleKeys = english[namespaces.default];
