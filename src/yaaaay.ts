import i18next from "https://deno.land/x/i18next@v20.2.2/index.js";
import {
  defaultLocale,
  defaultLocaleKeys,
  Locales,
  messages,
  namespaces,
} from "./locales.ts";

//#region types
// Source: https://stackoverflow.com/questions/58277973/how-to-type-check-i18n-dictionaries-with-typescript
type GetDictValue<T extends string, O> = T extends `${infer A}.${infer B}`
  ? A extends keyof O
    ? GetDictValue<B, O[A]>
    : never
  : T extends keyof O
  ? O[T]
  : never;

type GetStringDictValue<T extends string, O> = T extends `${infer A}.${infer B}`
  ? A extends keyof O
    ? GetDictValue<B, O[A]>
    : never
  : T extends keyof O
  ? never
  : never;

// deno-lint-ignore ban-types
type DeepKeys<T, S extends string> = T extends object
  ? S extends `${infer I1}.${infer I2}`
    ? I1 extends keyof T
      ? `${I1}.${DeepKeys<T[I1], I2>}`
      : keyof T & string
    : S extends keyof T
    ? `${S}`
    : keyof T & string
  : "";

type Keys<S extends string> = S extends ""
  ? []
  : S extends `${infer _}{{${infer B}}}${infer C}`
  ? [B, ...Keys<C>]
  : never;

type Interpolate<
  S extends string,
  I extends Record<Keys<S>[number], string>
> = S extends ""
  ? ""
  : S extends `${infer A}{{${infer B}}}${infer C}`
  ? `${A}${I[Extract<B, keyof I>]}${Interpolate<C, I>}`
  : never;

type translate = <
  S extends string,
  I extends Record<
    Keys<GetStringDictValue<S, typeof defaultLocaleKeys>>[number],
    string
  >
>(
  p: DeepKeys<typeof defaultLocaleKeys, S>,
  args?: I
) => Interpolate<GetStringDictValue<S, typeof defaultLocaleKeys>, I>;
//#endregion

const i18n = i18next.createInstance();
i18n.init({
  lng: defaultLocale,
  fallbackLng: defaultLocale,
  resources: messages,
});

// deno-lint-ignore no-explicit-any
const translate: translate = (p: any, args: any) => {
  const map = i18n.getFixedT(Locales.EN, namespaces.default);
  const translated = map(p, args);
  return translated;
};

const translated = translate("hi.person", { name: "Yui" } as const);
console.log(translated);
